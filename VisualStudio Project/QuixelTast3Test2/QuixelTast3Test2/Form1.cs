﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
 using System.IO.Pipes;
using System.IO;
using SharedMemory;
using System.IO.MemoryMappedFiles;
using System.Security.AccessControl;
using System.Security.Principal;

namespace QuixelTast3Test2
{
    public partial class Form1 : Form
    {
        struct messagedata
        {
            public char[] message;
        };
        public enum ConnectionState
        {
            Pipe, MMF
        };
        public ConnectionState CurrentConnecttionState = ConnectionState.Pipe;

        public bool isServer = false;
        public Form1()
        {
            InitializeComponent();
           // Chatbox.Text = "blablablaksdjfaksdjfasld";
        }
        
 
        //public static NamedPipeServerStream serverPipe = new NamedPipeServerStream("QuixelTest");
        //public static NamedPipeClientStream clientPipe = new NamedPipeClientStream("QuixelTest");
        NamedPipeServerStream pipeServer; 
        NamedPipeClientStream pipeClient;
        public void ConnectToServer()
        {
            if (CurrentConnecttionState == ConnectionState.Pipe)
            {

                notifucationText.Text = "Attempting to connect to pipe...";
            pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
            // Connect to the pipe or wait until the pipe is available.
            Console.WriteLine("Attempting to connect to pipe...");
            try
            {
                Console.WriteLine("pinocchio trying to connect to pipe");
                pipeClient.Connect(5000);
                Console.WriteLine("Connected to pipe");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.Message.Contains("cannot find") || e.Message.Contains("timed out"))
                {
                        notifucationText.Text = "No Pipe Exist.. Createing new pipe";
                        CreatePipe();
                }
            }
            }
            else if (CurrentConnecttionState == ConnectionState.MMF)
            {
                CreatOropenMemoryFile();
            }


        }
        private void button1_Click(object sender, EventArgs e)
        {
            // Write data to IPS
            string text = EnteredMessage.Text;
            if (CurrentConnecttionState == ConnectionState.Pipe)
            {

            if (isServer)
            {
                if (pipeServer == null)
                {
                    pipeServer = new NamedPipeServerStream("QuixelPipe", PipeDirection.InOut);
                }
                    try
                    {
                        // Read user input and send that to the client process.   
                    byte[] sendarray = Encoding.ASCII.GetBytes(text);
                    pipeServer.Write(sendarray, 0, sendarray.Length);
                    }
                    catch (IOException er)
                    {
                        Console.WriteLine("ERROR: {0}", er.Message);
                    }
                
            }
            else
            {
                if (pipeClient == null)
                {
                    pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
                }
               
                    if (!pipeClient.IsConnected)
                    {
                        pipeClient.Connect();
                    }
                try
                {
                    // Read user input and send that to the client process.   
                    byte[] sendarray = Encoding.ASCII.GetBytes(text);
                    pipeClient.Write(sendarray, 0, sendarray.Length);
                }
                catch (IOException er)
                {
                    Console.WriteLine("ERROR: {0}", er.Message);
                }
            }
            }
            else if (CurrentConnecttionState == ConnectionState.MMF)
            {
                SendDateViaMMF();
            }
        }

        public void CreatePipe()
        {
            Console.WriteLine("Creating Pipe");
            pipeServer = new NamedPipeServerStream("QuixelPipe", PipeDirection.InOut);
            Console.WriteLine("NamedPipeServerStream object created.");
                isServer = true;
                // Wait for a client to connect
                Console.WriteLine("Waiting for client connection...");
            notifucationText.Text = "plese connect your client.";
            pipeServer.WaitForConnection();
                notifucationText.Text = "Client connected.. try sending a message ";
                Console.WriteLine("Client connected");
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Read data 
            if (CurrentConnecttionState == ConnectionState.Pipe)
            {


                if (isServer)
                {
                    if (pipeServer != null)
                    {
                        byte[] bytesRecieved = new byte[1024];
                        pipeServer.Read(bytesRecieved, 0, 1024);
                        Console.WriteLine("bytes reading complete");
                        string Data = Encoding.ASCII.GetString(bytesRecieved, 0, bytesRecieved.Length);
                        Console.WriteLine(" data read  = " + Data);
                        Chatbox.Text += "/n " + Data;
                    }
                    else
                    {
                        Console.WriteLine("Pinocchio : Pipe not created");
                    }

                }
                else
                {
                    if (pipeClient == null)
                    {
                        pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
                    }
                    if (!pipeClient.IsConnected)
                    {
                        pipeClient.Connect();

                    }
                    Console.WriteLine("Attempting to Read from pipe...");
                    byte[] bytesRecieved = new byte[6];
                    pipeClient.Read(bytesRecieved, 0, 5);
                    Console.WriteLine("bytes reading complete");
                    string Data = Encoding.ASCII.GetString(bytesRecieved, 0, bytesRecieved.Length);
                    Console.WriteLine(" data read  = " + Data);
                    Chatbox.Text += "/n" + Data;
                }
            }
            else if (CurrentConnecttionState == ConnectionState.MMF) ;
            {
                if (mmf != null)
                {
                    MemoryMappedViewStream stream = mmf.CreateViewStream();
                    StreamReader sr = new StreamReader(stream);
                    string data = sr.ReadToEnd();
                    //  UnityEngine.Debug.LogError(sr.ReadToEnd());
                    Chatbox.Text += "/n " + data;

                }
                else
                {
                   Console.WriteLine("connect does not exixt please connet to file ");
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            notifucationText.Text = "Attempting to connect to pipe...";
            ConnectToServer();
        }
        protected MemoryMappedViewAccessor View;
        public MemoryMappedFileSecurity mSec;
        public MemoryMappedFile mmf;
        public void CreatOropenMemoryFile()
        {
            mSec = new MemoryMappedFileSecurity();
            mSec.AddAccessRule(new AccessRule<MemoryMappedFileRights>(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MemoryMappedFileRights.FullControl, AccessControlType.Allow));
            mmf = MemoryMappedFile.CreateOrOpen("Global\\QuixelMemoryFile", 100, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, mSec, HandleInheritability.Inheritable);
            View = mmf.CreateViewAccessor(0, 1024, MemoryMappedFileAccess.ReadWrite);

        }
        public void SendDateViaMMF()
        {
            if (mmf != null)
            {
                MemoryMappedViewStream stream = mmf.CreateViewStream();
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(EnteredMessage.Text);

            }
            else
            {
                CreatOropenMemoryFile();
            }
        }
        private void sharedmemorytest_Click(object sender, EventArgs e)
        {
            if (mmf!= null)
           {
                MemoryMappedViewStream stream = mmf.CreateViewStream();
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(EnteredMessage.Text);

            }
            else
            {
                CreatOropenMemoryFile();
            }
                
        }

        private void PipeRadioButtin_CheckedChanged(object sender, EventArgs e)
        {
          
            if (PipeRadioButtin.Checked)
            {
                CurrentConnecttionState = ConnectionState.Pipe;      
            }
          
        }

        private void MMFRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (MMFRadioButton.Checked)
            {
                CurrentConnecttionState = ConnectionState.MMF;
            }
        }   
    }
}
