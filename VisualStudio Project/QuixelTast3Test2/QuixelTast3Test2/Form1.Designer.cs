﻿namespace QuixelTast3Test2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.EnteredMessage = new System.Windows.Forms.TextBox();
            this.sharedmemorytest = new System.Windows.Forms.Button();
            this.PipeRadioButtin = new System.Windows.Forms.RadioButton();
            this.MMFRadioButton = new System.Windows.Forms.RadioButton();
            this.Chatbox = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.notifucationText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(459, 374);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 61);
            this.button1.TabIndex = 0;
            this.button1.Text = "send";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(605, 374);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 64);
            this.button2.TabIndex = 1;
            this.button2.Text = "Read";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(540, 205);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(145, 35);
            this.button3.TabIndex = 2;
            this.button3.Text = "Connect";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // EnteredMessage
            // 
            this.EnteredMessage.Location = new System.Drawing.Point(114, 374);
            this.EnteredMessage.Multiline = true;
            this.EnteredMessage.Name = "EnteredMessage";
            this.EnteredMessage.Size = new System.Drawing.Size(316, 61);
            this.EnteredMessage.TabIndex = 3;
            // 
            // sharedmemorytest
            // 
            this.sharedmemorytest.Location = new System.Drawing.Point(526, 40);
            this.sharedmemorytest.Name = "sharedmemorytest";
            this.sharedmemorytest.Size = new System.Drawing.Size(168, 45);
            this.sharedmemorytest.TabIndex = 4;
            this.sharedmemorytest.Text = "sharedmemory";
            this.sharedmemorytest.UseVisualStyleBackColor = true;
            this.sharedmemorytest.Click += new System.EventHandler(this.sharedmemorytest_Click);
            // 
            // PipeRadioButtin
            // 
            this.PipeRadioButtin.AutoSize = true;
            this.PipeRadioButtin.Location = new System.Drawing.Point(540, 127);
            this.PipeRadioButtin.Name = "PipeRadioButtin";
            this.PipeRadioButtin.Size = new System.Drawing.Size(73, 17);
            this.PipeRadioButtin.TabIndex = 5;
            this.PipeRadioButtin.TabStop = true;
            this.PipeRadioButtin.Text = "Use Pipes";
            this.PipeRadioButtin.UseVisualStyleBackColor = true;
            this.PipeRadioButtin.CheckedChanged += new System.EventHandler(this.PipeRadioButtin_CheckedChanged);
            // 
            // MMFRadioButton
            // 
            this.MMFRadioButton.AutoSize = true;
            this.MMFRadioButton.Location = new System.Drawing.Point(540, 163);
            this.MMFRadioButton.Name = "MMFRadioButton";
            this.MMFRadioButton.Size = new System.Drawing.Size(145, 17);
            this.MMFRadioButton.TabIndex = 6;
            this.MMFRadioButton.TabStop = true;
            this.MMFRadioButton.Text = "Use Memory Mapped File";
            this.MMFRadioButton.UseVisualStyleBackColor = true;
            this.MMFRadioButton.CheckedChanged += new System.EventHandler(this.MMFRadioButton_CheckedChanged);
            // 
            // Chatbox
            // 
            this.Chatbox.Location = new System.Drawing.Point(114, 53);
            this.Chatbox.Multiline = true;
            this.Chatbox.Name = "Chatbox";
            this.Chatbox.ReadOnly = true;
            this.Chatbox.Size = new System.Drawing.Size(316, 289);
            this.Chatbox.TabIndex = 7;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // notifucationText
            // 
            this.notifucationText.Location = new System.Drawing.Point(459, 269);
            this.notifucationText.Name = "notifucationText";
            this.notifucationText.ReadOnly = true;
            this.notifucationText.Size = new System.Drawing.Size(329, 20);
            this.notifucationText.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.notifucationText);
            this.Controls.Add(this.Chatbox);
            this.Controls.Add(this.MMFRadioButton);
            this.Controls.Add(this.PipeRadioButtin);
            this.Controls.Add(this.sharedmemorytest);
            this.Controls.Add(this.EnteredMessage);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox EnteredMessage;
        private System.Windows.Forms.Button sharedmemorytest;
        private System.Windows.Forms.RadioButton PipeRadioButtin;
        private System.Windows.Forms.RadioButton MMFRadioButton;
        private System.Windows.Forms.TextBox Chatbox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox notifucationText;
    }
}

