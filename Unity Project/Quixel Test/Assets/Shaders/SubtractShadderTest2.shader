﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Test2Sub"
{
    Properties
    {
      
		_texture1("TextureA",2D) = "white"{}
		_texture2("TextureB",2D) = "white"{}
		_texture3("Res",2D) = "white"{}
		
    }
  
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex VertexPass
			#pragma fragment fregmentPass
			struct appData
			{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			};


			struct v2f
			{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
			};

			v2f VertexPass(appData v)
			{
			v2f var;
			var.vertex =UnityObjectToClipPos(v.vertex);
			var.uv = v.uv;
			return var;
			}
			sampler2D TextureA;
			sampler2D TextureB;
			sampler2D Res;
			fixed4 fregmentPass(v2f i) : SV_Target
			{
			fixed4 col = tex2D(TextureA, i.uv) - tex2D(TextureB,i.uv);
			//Res = (tex2D(Res,i.uv)) * col;
			return col;
			}
			ENDCG
        }

		
	}
    
}
