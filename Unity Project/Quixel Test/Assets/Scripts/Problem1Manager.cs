﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Problem1Manager : MonoBehaviour
{
    public GameObject sampleObject; // runtime object
    public GameObject[]  samplePrefabs;// objects to instantiate
    public GameObject spotLight;
    int generatedPrefabIndex = 0;
    public Vector2 mouseStartPosition; // used to calculate difference i.e how much of a mouse is dragged


    public bool CTRLAndLeftMouse = false, CTRLAndRightMouse = false, ALTAndLeftMouse = false; // to check which controll is active Also use to override and other controlls
    
    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main.gameObject;
    }
    public void CreateGameobject()
    {
        if (sampleObject)
        {
            Destroy(sampleObject);
        }
        sampleObject = GameObject.Instantiate(samplePrefabs[generatedPrefabIndex], Vector3.zero, Quaternion.identity);
        generatedPrefabIndex++;
        if (generatedPrefabIndex >= samplePrefabs.Length)
        {
            generatedPrefabIndex = 0;
        }

    }
    public IEnumerator BouncePolygon(GameObject Poly,int[] orignalTries)
    {
        bool animate = true;
        bool pop = true;
        float popSpeed = 5;
        while (animate)
        {
            if (pop)
            {
                Poly.transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime)*popSpeed;
                Poly.transform.forward += (Poly.transform.forward *Time.deltaTime * popSpeed);
                if (Poly.transform.localScale.x>5f)
                {
                    pop = false;
                }

            }
            else
            {
                Poly.transform.localScale -= new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime) * popSpeed;
                Poly.transform.forward -= (Poly.transform.forward * Time.deltaTime * popSpeed);
                if (Poly.transform.localScale.x < 1f)
                {
                    animate = false;
                    Poly.transform.localScale = Vector3.one;
                }
            }
            yield return new WaitForEndOfFrame();
        }
        Mesh orignal = sampleObject.GetComponent<MeshFilter>().mesh;
        Destroy(Poly); 
        orignal.triangles = orignalTries;
       
        yield return 0;
    }
    public void ReJoinMesh(int[] detachedTries , int index)
    {
        Debug.LogError(" Selected index in rejoining" + index);
        Mesh subtractedmesh = sampleObject.GetComponent<MeshFilter>().mesh;
        int[] subtractedTris = subtractedmesh.triangles;
        int[] orignal = new int[subtractedmesh.triangles.Length + 3];

        //Debug.LogError("subtracted mesh tri count " + subtractedTris.Length);
        //Debug.LogError("orignal mesh tri count " + orignal.Length);
        //Debug.LogError("detached mesh tri count " + detachedTries.Length);
        int i = 0, j = 0;
        while (i < subtractedmesh.triangles.Length)
        {
            if (j != index * 3)
            {
                orignal[i++] = subtractedTris[j++];
                orignal[i++] = subtractedTris[j++];
                orignal[i++] = subtractedTris[j++];
            }
            else
            {
                Debug.LogError("index value of i & J Respectively" + i + " & " + j);
                Debug.Break();
                orignal[i++] = detachedTries[0];
                orignal[i++] = detachedTries[1];
                orignal[i++] = detachedTries[2];
            }
        }
        subtractedmesh.triangles = orignal;

    }
    public void GetSelectedPolygon(int seletedIndex)
    {
        Mesh orignal = sampleObject.GetComponent<MeshFilter>().mesh;
        int[] orignalTries = orignal.triangles;
        int[] subtractedTries = new int[orignal.triangles.Length-3];
        int[] selectedTris = new int[3];
        int i = 0, j = 0;
        while (j < orignal.triangles.Length)
        {
            if (j  != seletedIndex * 3)
            {
                subtractedTries[i++] = orignalTries[j++];
                subtractedTries[i++] = orignalTries[j++];
                subtractedTries[i++] = orignalTries[j++];
            }
            else
            {
                Debug.LogError("Selected index " + seletedIndex);
                selectedTris[0] = orignalTries[j++];
                selectedTris[1] = orignalTries[j++];
                selectedTris[2] = orignalTries[j++];
            }
        }
        GameObject SelectedPolygon = new GameObject("mesh" , typeof (MeshFilter) , typeof(MeshRenderer));



        orignal.triangles = subtractedTries ;
        //Mesh selectedPolyMesh = orignal;
        Mesh selectedPolyMesh = new Mesh();
        selectedPolyMesh.vertices = orignal.vertices;
        selectedPolyMesh.uv = orignal.uv;
        selectedPolyMesh.triangles = selectedTris;
        SelectedPolygon.GetComponent<MeshFilter>().mesh = selectedPolyMesh;
        
         

        MeshRenderer orignalMeshRenderer = sampleObject.GetComponent<MeshRenderer>();
        MeshRenderer selectedMeshRenderer = SelectedPolygon.GetComponent<MeshRenderer>();
        selectedMeshRenderer.material = orignalMeshRenderer.material;
       // 
        StartCoroutine( BouncePolygon(SelectedPolygon,orignalTries));
        
    }
    public float zoomSpeed;
    public float rotationSpeed;
    public GameObject camera;
    // Update is called once per frame
    void Update()
    {
       

        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            if (Input.GetMouseButtonDown(0))
            {
                CTRLAndLeftMouse = true;
                CTRLAndRightMouse = false;
                ALTAndLeftMouse = false;
                mouseStartPosition = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
               
            }
            if (Input.GetMouseButtonUp(0))
            {
                CTRLAndLeftMouse = false;
            }


            if (Input.GetMouseButtonDown(1))
            {
                mouseStartPosition = Input.mousePosition;
                CTRLAndRightMouse = true;
                CTRLAndLeftMouse = false;
                ALTAndLeftMouse = false;
            }
            if (Input.GetMouseButton(1))
            {

            }
            if (Input.GetMouseButtonUp(1))
            {
                CTRLAndRightMouse = false;
            }
        }
        else if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        {

            if (Input.GetMouseButtonDown(0))
            {
                mouseStartPosition = Input.mousePosition;
                ALTAndLeftMouse = true;
                CTRLAndRightMouse = false;
                CTRLAndLeftMouse = false;
            }
            if (Input.GetMouseButton(0))
            {
              
            }
            if (Input.GetMouseButtonUp(0))
            {
                ALTAndLeftMouse = false;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hitObject;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hitObject, 1000f))
                {
                    GetSelectedPolygon(hitObject.triangleIndex);
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                CTRLAndLeftMouse = false;
                ALTAndLeftMouse = false;
            }
            if (Input.GetMouseButtonUp(1))
            {
                CTRLAndRightMouse = false;
              
            }

        }

        if (CTRLAndLeftMouse)
        {
            ZoomControll();
        }
        if (CTRLAndRightMouse)
        {
            RotateControll();
        }
        if (ALTAndLeftMouse)
        {
            RotateTourch();
        }

    }


    public void ZoomControll()
    {
        if (sampleObject)
        {
           
            float deltaY =   Input.mousePosition.y -mouseStartPosition.y;
            float deltaZoom = deltaY * zoomSpeed * Time.deltaTime;
            if (deltaZoom < 0)
            {
               
                if (Vector3.Distance(camera.transform.position, sampleObject.transform.position) < 25f)
                {
                    camera.transform.position = Vector3.MoveTowards(camera.transform.position, sampleObject.transform.position, deltaZoom);
                   
                }
            }
            else if (deltaZoom > 0)
            {
               
                if (Vector3.Distance(camera.transform.position, sampleObject.transform.position) > 3f)
                {
                    camera.transform.position = Vector3.MoveTowards(camera.transform.position, sampleObject.transform.position, deltaZoom);
                   
                }
            }
            mouseStartPosition = Input.mousePosition;


        }
    }
    public void RotateControll()
    {
        if (sampleObject)
        {
            float deltaY = Input.mousePosition.y - mouseStartPosition.y;
            float deltaX  = Input.mousePosition.x - mouseStartPosition.x;
            float rotationDeltaX = deltaY * rotationSpeed * Time.deltaTime;
            float rotationDeltaY = deltaX * rotationSpeed * Time.deltaTime;
            sampleObject.transform.localEulerAngles += new Vector3(rotationDeltaX, rotationDeltaY, 0) ;
            mouseStartPosition = Input.mousePosition;
        }
    }
    public void RotateTourch()
    {
        if (spotLight)
        {
            float deltaY = Input.mousePosition.y - mouseStartPosition.y;
            float deltaX = Input.mousePosition.x - mouseStartPosition.x;
            float rotationDeltaX = deltaY * rotationSpeed * Time.deltaTime;
            float rotationDeltaY = deltaX * rotationSpeed * Time.deltaTime;
            spotLight.transform.localEulerAngles += new Vector3(rotationDeltaX, rotationDeltaY, 0);
            mouseStartPosition = Input.mousePosition;
        }
       
    }
    public void OnIntensityValueChanged(Slider slider)
    {
        spotLight.GetComponent<Light>().intensity = slider.value;
    }
}




