﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Problem2Manager : MonoBehaviour
{
    public RenderTexture t1, t2;
    public Texture TextureA, TextureB = null;
    public ComputeShader subShader;
    public RenderTexture Result;
    public Color pickcolor;
    public GameObject planer,planerA,planerB;

    // Start is called before the first frame update
    void Start()
    {
        // texRef is your Texture2D
        // You can also reduice your texture 2D that way
       
        ReadDataFromPlist();
    }
    public void LoadTexture1()
    {
        TextureA = Resources.Load(allTextureList[0].path) as Texture;
        planerA.GetComponent<Renderer>().material.mainTexture = TextureA;
    }
    public void LoadTexture2()
    {
        TextureB = Resources.Load(allTextureList[1].path) as Texture;
        planerB.GetComponent<Renderer>().material.mainTexture = TextureB;
    }
    public void SubtractTextures()
    {
        t1 = new RenderTexture(TextureA.width / 2, TextureA.height / 2, 0);
        RenderTexture.active = t1;
        // Copy your texture ref to the render texture
        Graphics.Blit(TextureA, t1);

        t2 = new RenderTexture(TextureB.width / 2, TextureB.height / 2, 0);
        RenderTexture.active = t2;
        // Copy your texture ref to the render texture
        Graphics.Blit(TextureB, t2);
        Material mat = planer.GetComponent<MeshRenderer>().material;
        mat.SetTexture("TextureA", t1);
        mat.SetTexture("TextureB", t2);
        Texture Temp = mat.mainTexture;
        if (Temp == null)
        {
            Debug.Log("no texture data");
        }
        byte[] bytes = ((Texture2D)Temp).EncodeToPNG();
        var dirPath = Application.dataPath + "/ResTexturesSaved";
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
            Debug.Log("directory created At " + dirPath);
        }
        Debug.Log("directory created At " + dirPath);
        if (bytes == null)
        {
            Debug.Log("no bytes data");


        }
        File.WriteAllBytes(dirPath + "ResTex" + ".png", bytes);
    }
    // Update is called once per frame
    void Update()
    {

        
    }
    public List<TextureInfo> allTextureList = new List<TextureInfo>();
    void ReadDataFromPlist()
    {
        Hashtable gameDataPlistFile = new Hashtable();
        //Reading the game configurations
        TextAsset myPlist = (TextAsset)Resources.Load("TextureInfoQuixelText");
        Debug.LogError(myPlist.text);
        if (PlistManager.ParsePListFile(myPlist.text, ref gameDataPlistFile))
        {

        }

        Hashtable textureData = gameDataPlistFile["TextureData"] as Hashtable;
        foreach (DictionaryEntry item in textureData)
        {
            allTextureList.Add(new TextureInfo((item.Value as Hashtable)));
        }

        //tesing 
        foreach (var item in allTextureList)
        {
            Debug.LogError(item.path);
        }
    }
    public class TextureInfo
    {
        public string name
        { get; set; }
        public string path
        { get; set; }
        public TextureInfo(Hashtable hash)
        {
            name = (string)hash["Name"];
            path = (string)hash["Path"];
        }
    }
}
