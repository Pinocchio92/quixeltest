﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using System.IO;
using System.Diagnostics;
using UnityEngine;
using System;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SharedMemory;
using System.IO.MemoryMappedFiles;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;

public class problem3Manager : MonoBehaviour
{
    public enum ConnectionState
    {
        Pipe,MMF
    };
    public bool isconnected = false;
    public ConnectionState CurrentConnecttionState = ConnectionState.Pipe;
    protected MemoryMappedViewAccessor View;
    public Text chatBox;
    //private NamedPipeServerStream pipeSerwer = new NamedPipeServerStream("QuixelTest");
    //private NamedPipeClientStream PiperClient = new NamedPipeClientStream("QuixelTest");
    public bool isServer = false;
    public InputField enteredssageTextBox;
    public Button sendBtn,ConnectBtn;
    void Update()
    {
        if (isconnected)
        {
            if (isServer)
            {
                UnityEngine.Debug.LogError(" Can read data " + pipeServer.CanRead);
            }
            else
            {
                UnityEngine.Debug.LogError(" Can read data " + pipeClient.CanRead);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //DisableSendAndInputField();    
    }
    public StreamWriter sw;

    public void readData()
    {
        if (CurrentConnecttionState == ConnectionState.Pipe)
        {

       
        if (isServer)
        {
            if (pipeServer != null)
            {
                byte[] bytesRecieved = new byte[6];
                pipeServer.Read(bytesRecieved, 0, 5);
                UnityEngine.Debug.LogError("bytes reading complete");
                string Data = Encoding.ASCII.GetString(bytesRecieved, 0, bytesRecieved.Length);
                UnityEngine.Debug.LogError(" data read  = " + Data);
                    chatBox.text += "/n " + Data;

                }
            else
            {
                UnityEngine.Debug.LogError("Pinocchio : Pipe not created" );
            }            
           
        }
        else
        {
            if (pipeClient == null)
            {
                pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
            }
            if (!pipeClient.IsConnected)
            {
                pipeClient.Connect();

            }
            UnityEngine.Debug.LogError("Attempting to Read from pipe...");
            byte[] bytesRecieved = new byte[1024];
            pipeClient.Read(bytesRecieved, 0, bytesRecieved.Length);
            UnityEngine.Debug.LogError("bytes reading complete");
            string Data = Encoding.ASCII.GetString(bytesRecieved, 0, bytesRecieved.Length);
            UnityEngine.Debug.LogError(" data read  = " + Data);
                chatBox.text += "/n " + Data;    
        }
        }
        else if (CurrentConnecttionState == ConnectionState.MMF)
        {
            // mmf = MemoryMappedFile.CreateOrOpen("Global\\QuixelMemoryFile", 100, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, HandleInheritability.Inheritable);
            // View = mmf.CreateViewAccessor(0, 1024, MemoryMappedFileAccess.ReadWrite);
            if (mmf != null)
            {
                MemoryMappedViewStream stream = mmf.CreateViewStream();
                StreamReader sr = new StreamReader(stream);
                string data = sr.ReadToEnd();
              //  UnityEngine.Debug.LogError(sr.ReadToEnd());
                chatBox.text += "/n " + data;

            }
            else
            {
                UnityEngine.Debug.LogError("connect does not exixt please connet to file ");
            } 
           
        }

    }

        // Update is called once per frame
    
    NamedPipeServerStream pipeServer;
    NamedPipeClientStream pipeClient;
    public void ConnectToServer()
    {
        if (CurrentConnecttionState == ConnectionState.Pipe)
        {

       
        UnityEngine.Debug.LogError("connecting to server initializing pipe sequence");

        pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
       // Connect to the pipe or wait until the pipe is available.
            UnityEngine.Debug.LogError("Attempting to connect to pipe...");
        try
        {
            Console.WriteLine("pinocchio trying to connect to pipe");
            pipeClient.Connect(5000);
            Console.WriteLine("Connected to pipe");
            DisableSendAndInputField(true);
                isconnected = true;
                //Task.Factory.StartNew(() =>
                //{
                //    while (true)
                //    {
                //        byte[] bytesRecieved = new byte[1024];
                //        pipeClient.Read(bytesRecieved, 0, bytesRecieved.Length);
                //        string Data = Encoding.ASCII.GetString(bytesRecieved, 0, bytesRecieved.Length);
                //        UnityEngine.Debug.LogError(" data read  = " + Data);
                //    }
                //});
            }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            if (e.Message.Contains("cannot find") || e.Message.Contains("timed out"))
            {
                CreatePipe();
            }
        }

        UnityEngine.Debug.LogError("Press Enter to continue...");
        }
        else if (CurrentConnecttionState == ConnectionState.MMF)
        {
           // UnityEngine.Debug.LogError("MMF created");
            CreatOropenMemoryFile();
        }
    }
    public void sendString()
    {
        string text = enteredssageTextBox.text;
        if (CurrentConnecttionState == ConnectionState.Pipe)
        {

        
        if (isServer)
        {
            if (pipeServer == null)
            {
                pipeServer = new NamedPipeServerStream("QuixelPipe", PipeDirection.InOut);
            }
            try
            {
                // Read user input and send that to the client process.   
                byte[] sendarray = Encoding.ASCII.GetBytes(text);
                pipeServer.Write(sendarray, 0, sendarray.Length);
            }
            catch (IOException er)
            {
                Console.WriteLine("ERROR: {0}", er.Message);
            }

        }
        else
        {
            if (pipeClient == null)
            {
                pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
            }

            if (!pipeClient.IsConnected)
            {
                pipeClient.Connect();
            }
            try
            {
                // Read user input and send that to the client process.   
                byte[] sendarray = Encoding.ASCII.GetBytes(text);
                pipeServer.Write(sendarray, 0, sendarray.Length);
            }
            catch (IOException er)
            {
                Console.WriteLine("ERROR: {0}", er.Message);
            }
        }
        }
        else if (CurrentConnecttionState == ConnectionState.MMF)
        {

        }
    }
    public void CreatePipe()
    {
        UnityEngine.Debug.LogError("Creating Pipe");
        pipeServer = new NamedPipeServerStream("QuixelPipe", PipeDirection.InOut);
        UnityEngine.Debug.LogError("NamedPipeServerStream object created.");
        isServer = true;
        // Wait for a client to connect
        UnityEngine.Debug.LogError("Waiting for client connection...");
        pipeServer.WaitForConnection();

        UnityEngine.Debug.LogError("Client connected");
        DisableSendAndInputField(true);
        isconnected = true;
        
    }
   
  
    public MemoryMappedFile mmf;
    public void CreatOropenMemoryFile()
    {
       
       mmf = MemoryMappedFile.CreateOrOpen("Global\\QuixelMemoryFile", 100, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, HandleInheritability.Inheritable);
        View = mmf.CreateViewAccessor(0, 1024, MemoryMappedFileAccess.ReadWrite);
        UnityEngine.Debug.LogError("MMF created");
    }
    private void button2_Click(object sender, EventArgs e)
    {
        if (isServer)
        {
            using (pipeServer)
            {
                if (pipeServer == null)
                {
                    pipeServer = new NamedPipeServerStream("testpipe", PipeDirection.InOut);
                }
                using (StreamReader sr = new StreamReader(pipeServer))
                {
                    // Display the read text to the console
                    string temp;
                    while ((temp = sr.ReadLine()) != null)
                    {
                        Console.WriteLine("Received from server: {0}", temp);
                        Console.WriteLine(temp);
                    }

                }

            }
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();
        }
        else
        {
            // Connect to the pipe or wait until the pipe is available.
            Console.WriteLine("Attempting to connect to pipe...");
            if (pipeClient == null)
            {
                pipeClient = new NamedPipeClientStream(".", "QuixelPipe", PipeDirection.InOut);
            }
            if (!pipeClient.IsConnected)
            {
                pipeClient.Connect();

            }


            Console.WriteLine("Connected to pipe.");
            //Console.WriteLine("There are currently {0} pipe server instances open.",
            //   pipeClient.NumberOfServerInstances);
            using (StreamReader sr = new StreamReader(pipeClient))
            {
                // Display the read text to the console
                string temp;
                while ((temp = sr.ReadLine()) != null)
                {
                    Console.WriteLine("Received from server: {0}", temp);
                    Console.WriteLine(temp);
                }

            }
        }
    }
    public void DisableSendAndInputField( bool state = false)
    {
        enteredssageTextBox.interactable = state;
        sendBtn.interactable = state;
    }
    public void DropDownValueChanged(Dropdown d)
    {
        if (d.value == 0)
        {
            CurrentConnecttionState = ConnectionState.Pipe;
        }
        else if (d.value == 1) 
        {
            CurrentConnecttionState = ConnectionState.MMF;
        }
    }
}
